package br.com.itau.boleto.controllers;

import br.com.itau.boleto.DTOs.CompraDTO;
import br.com.itau.boleto.models.Compra;
import br.com.itau.boleto.services.CompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/compras")
public class CompraController {

    @Autowired
    private CompraService compraService;

    @PostMapping("/{id_parceiro}/{id_produto}/{id_cliente}")
    @ResponseStatus(HttpStatus.CREATED)
    public Compra cadastrarCompra(@RequestBody @Valid CompraDTO compraDTO, @PathVariable(name = "id_parceiro") int id_parceiro,
                                  @PathVariable(name = "id_produto") int id_produto, @PathVariable(name = "id_cliente") int id_cliente) {
        return compraService.novaCompra(compraDTO, id_parceiro, id_produto, id_cliente);
    }


    @GetMapping
    public Iterable<Compra> lerTodasAsCompras() {
        return compraService.lerTodasAsCompras();
    }

    @GetMapping("/{id}")
    public Compra pesquisarPorId(@PathVariable(name = "id") int id) {
        try {
            Compra compra = compraService.buscarCompraPorId(id);
            return compra;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
