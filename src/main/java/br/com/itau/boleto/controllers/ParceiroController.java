package br.com.itau.boleto.controllers;

import br.com.itau.boleto.models.Parceiro;
import br.com.itau.boleto.services.ParceiroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/parceiros")
public class ParceiroController {

    @Autowired
    private ParceiroService parceiroService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    //Cadastrar
    public Parceiro cadastrarParceiro(@RequestBody @Valid Parceiro parceiro) {
        Parceiro objetoParceiro = parceiroService.salvarParceiro(parceiro);
        return objetoParceiro;
    }


    //Buscar por Id
    @GetMapping("/{id}")
    public Parceiro pesquisarPorId(@PathVariable(name = "id") int id){
        try{
            Parceiro parceiro = parceiroService.buscarParceiroPorId(id);
            return parceiro;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //Atualizar
    @PutMapping("/{id}")
    public Parceiro atualizarParceiro(@RequestBody @Valid Parceiro parceiro, @PathVariable(name = "id") int id){
        try {
            Parceiro parceiroDB = parceiroService.atualizarParceiro(id, parceiro);
            return parceiroDB;
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //Deletar
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarParceiro(@PathVariable(name = "id") int id){
        try{
            parceiroService.deletarParceiro(id);
        }catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}

