package br.com.itau.boleto.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    private static final String SEGREDO = "liubvwt76";

    private static Long TEMPO_DE_VALIDADE = 2880000000l;

    public String gerarToken(String email) {
        Date dataVencimento = new Date(System.currentTimeMillis() + TEMPO_DE_VALIDADE);
        String token = Jwts.builder().setSubject(email)
                .setExpiration(dataVencimento)
                .signWith(SignatureAlgorithm.HS512, SEGREDO.getBytes()).compact();
        return token;
    }

    public String getUserName(String token){
        Claims claims = getClaims(token);
        String email = claims.getSubject();
        return email;
    }

    public Claims getClaims(String token){
        return Jwts.parser().setSigningKey(SEGREDO.getBytes())
                .parseClaimsJws(token).getBody();
    }

    public boolean tokenValido(String token) {
        Claims claims = getClaims(token);

        String email = claims.getSubject();
        Date dataDeExpiracao = claims.getExpiration();
        Date dataAtual = new Date(System.currentTimeMillis());

        if (email != null && dataDeExpiracao != null && dataAtual.before(dataDeExpiracao)){
            return true;
        } else {
            return false;
        }
    }


}
