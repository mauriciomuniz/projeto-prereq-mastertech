package br.com.itau.boleto.enums;

public enum Situacao {
    PENDENTE_DE_PAGAMENTO, EM_ATRASO, PAGO
}
