package br.com.itau.boleto.repositories;

import br.com.itau.boleto.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {

}
