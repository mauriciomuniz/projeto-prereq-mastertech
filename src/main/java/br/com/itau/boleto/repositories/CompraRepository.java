package br.com.itau.boleto.repositories;

import br.com.itau.boleto.models.Compra;
import org.springframework.data.repository.CrudRepository;

public interface CompraRepository extends CrudRepository<Compra, Integer> {
    //int findCompraIdByBoleto_Id(int id_boleto);
}
