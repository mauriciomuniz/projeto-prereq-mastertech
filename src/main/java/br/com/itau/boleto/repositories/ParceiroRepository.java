package br.com.itau.boleto.repositories;

import br.com.itau.boleto.models.Compra;
import br.com.itau.boleto.models.Parceiro;
import org.springframework.data.repository.CrudRepository;

public interface ParceiroRepository extends CrudRepository<Parceiro, Integer> {

    Parceiro findById(String id);
}
