package br.com.itau.boleto.repositories;

import br.com.itau.boleto.models.Boleto;
import org.springframework.data.repository.CrudRepository;

public interface BoletoRepository extends CrudRepository<Boleto, Integer> {
    Boleto findByCodigoDeBarras(String codigoDeBarras);
}
