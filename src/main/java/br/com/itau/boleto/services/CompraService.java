package br.com.itau.boleto.services;

import br.com.itau.boleto.DTOs.CompraDTO;
import br.com.itau.boleto.enums.Situacao;
import br.com.itau.boleto.models.Boleto;
import br.com.itau.boleto.models.Compra;
import br.com.itau.boleto.repositories.CompraRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CompraService {

    @Autowired
    private CompraRepository compraRepository;

    @Autowired
    private BoletoService boletoService;

    public Compra novaCompra(CompraDTO compraDTO, int id_parceiro, int id_produto, int id_cliente) {
        Compra compra = compraDTO.converterParaCompra();
        List<Boleto> boletos = new ArrayList<>();
        double valorBoleto = compra.getValorDaCompra().doubleValue() / compra.getQuantidadeParcelas();

        // criar consulta para o produto e verificar qtd de parcelas

        for (int i = 0; i < compra.getQuantidadeParcelas(); i++) {
            Boleto boleto = boletoService.gerarNovoBoleto(id_parceiro, id_produto, id_cliente, valorBoleto, i + 1);
            boletos.add(boleto);
        }

        compra.setBoletos(boletos);
        compra.setSituacao(Situacao.PENDENTE_DE_PAGAMENTO);

        return compraRepository.save(compra);
    }


    public Compra buscarCompraPorId(int id) {
        Optional<Compra> compraOptional = compraRepository.findById(id);

        if (compraOptional.isPresent()) {
            Compra compra = compraOptional.get();
            return compra;
        } else {
            throw new RuntimeException("A compra não foi encontrado");
        }
    }

    public Iterable<Compra> lerTodasAsCompras() {
        return compraRepository.findAll();

    }

    public Compra atualizarCompra(int id_boleto){
        int compra_id = 1;//compraRepository.findCompraIdByBoleto_Id(id_boleto);
        Compra compra = buscarCompraPorId(compra_id);
        List<Boleto> boletos = compra.getBoletos();
        for (Boleto boleto :
             boletos) {
            if(boleto.getSituacao() != Situacao.PAGO){
                if(boleto.getSituacao() == Situacao.EM_ATRASO){
                    compra.setSituacao(Situacao.EM_ATRASO);
                    break;
                }
            }
        }
        return compraRepository.save(compra);
    }

}
